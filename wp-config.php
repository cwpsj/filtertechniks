<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'filtertechniks');

/** MySQL database username */
define('DB_USER', 'd815cd5d2261');

/** MySQL database password */
define('DB_PASSWORD', '41f1116519d1f864');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WN<uz|_J;}Wl`yWpF|9flO8=Zks:*IVM~&Zh0,ih4*9e)h^?>U=wo_n<644iYEbc');
define('SECURE_AUTH_KEY',  '8|-CoCGPd@mtbw[,+^rf1OM-jkNpTwH45Cwm}Ds?O,*6P:-t>mO?ujB-myY]u7|Q');
define('LOGGED_IN_KEY',    'C#3hn?M^4w8-^*r4O~Uh;!JugVz^@}E.,9`G`wdz2b=ZJb*I2s/Q5$C8CDS5A}[M');
define('NONCE_KEY',        'pw;s9YW~weU5B[i[p1oWQIah$ia0[x8D4H/xiJ[Mw@lU?a}uMGg:?@>+G$Ok)AFo');
define('AUTH_SALT',        '}&X28z|whivmJ!(<ic>~BH= ?wTz93yQq=gRGxYZoH{BkkX{r7bmLQnW8.Bu*/<Q');
define('SECURE_AUTH_SALT', 'kG(z=^bzyQ!qbE#WQgfLV&Cm-mz.-[Z-5Yql+6zU5$U0<V=%JpCZWBt+M3}#hfFz');
define('LOGGED_IN_SALT',   'u|*VOj!}^*^ZZY~Hq%26-2RZj80l,Clk8dn4`-Er2n`49J=A&!B/ wwL#5Z1G8$a');
define('NONCE_SALT',       'x3o-wy{7Ygbwc]F9_Pk;6icJK-0QBFZ-#nAl}PNDX!{whV(CH$57!mjr2`w$_3a>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
